BIN_NAME	= hello
SRCS		= main.c
OBJS		= $(SRCS:.c=.o)

CC		= gcc
LD		= ld
STRIP		= strip
RM		= rm -f

CFLAGS		= -Os -g
LDFLAGS		= -nostdlib -nostartfiles

$(BIN_NAME):
	$(CC) $(CFLAGS) -c $(SRCS)
	$(LD) $(LDFLAGS) $(OBJS) -o $(BIN_NAME)
	$(STRIP) $(BIN_NAME)

all:	$(BIN_NAME)

clean:
	$(RM) $(OBJS)

fclean:	clean
	$(RM) $(BIN_NAME)

re:	fclean all

.PHONY:	all clean fclean re

