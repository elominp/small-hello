static const char * hello = "\n"\
"Hello from Docker!\n"\
"This message shows that your installation appears to be working correctly.\n"\
"\n"\
"To generate this message, Docker took the following steps:\n"\
" 1. The Docker client contacted the Docker daemon.\n"\
" 2. The Docker daemon pulled the \"hello-world\" image from the Docker Hub.\n"\
"    (amd64)\n"\
" 3. The Docker daemon created a new container from that image which runs the\n"\
"    executable that produces the output you are currently reading.\n"\
" 4. The Docker daemon streamed that output to the Docker client, which sent it\n"\
"    to your terminal.\n"\
"\n"\
"To try something more ambitious, you can run an Ubuntu container with:\n"\
" $ docker run -it ubuntu bash\n"\
"\n"\
"Share images, automate workflows, and more with a free Docker ID:\n"\
" https://hub.docker.com/\n"\
"\n"\
"For more examples and ideas, visit:\n"\
" https://docs.docker.com/get-started/\n"\
"\n";

#if defined(__i386__)
#define AX "eax"
#define BX "ebx"
#define CX "ecx"
#define DX "edx"
#define SP "esp"
#define SYSCALL "int $0x80"
#elif defined(__x86_64)
#define AX "rax"
#define BX "rbx"
#define CX "rcx"
#define DX "rdx"
#define SP "rsp"
#define SYSCALL "int $0x80"
#endif

#define size_t int

static int _strlen(const char *str) {
    int i = 0;
    while (str[i++] != '\0');
    return i - 1;
}

static void _write(int fd, const void *buf, size_t count) {
    asm volatile (
        "\n\tmov $4, %%"AX";"
        "\n\tmov %0, %%"BX";"
        "\n\tmov %1, %%"CX";"
        "\n\tmov %2, %%"DX";"
        "\n\t"SYSCALL";"
        :
        : "m"(fd), "m"(buf), "m"(count)
    );
}

static void _puts(const char *str) {
    _write(1, str, _strlen(str));
}

static inline void exit(int __status) {
    asm volatile (
        "\n\tmov $1,%%"AX";"
        "\n\tmov %0,%%"BX";"
        "\n\t"SYSCALL";"
        :: "m"(__status)
    );
}

void _start() {
    _puts(hello);
    exit(0);
}
